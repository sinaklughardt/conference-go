import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            message = json.loads(body)
            presenter_name = message["presenter_name"]
            presenter_email = message["presenter_email"]
            presenter_title = message["presenter_title"]
            print(message)
            # presenter_title = message["title"]
            body = f"{presenter_name} we're happy to tell you that your presentation {presenter_title} has been accepted"
            send_mail(
                "Your presentation got approved",
                body,
                "admin@conference.go",
                [presenter_email],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            message = json.loads(body)
            presenter_name = message["presenter_name"]
            presenter_email = message["presenter_email"]
            presenter_title = message["presenter_title"]
            body = f"{presenter_name} unfortunately {presenter_title} has been rejected"
            send_mail(
                "Your presentation got rejected",
                body,
                "admin@conference.go",
                [presenter_email],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="approval")
        channel.basic_consume(
            queue="approval",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="rejections")
        channel.basic_consume(
            queue="rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
